#!/usr/bin/env python
# coding: utf-8

# # Game of Thrones Analysis 
# #By- Aarush Kumar
# #Dated: July 02,2021

# In[2]:


from IPython.display import Image
Image(url='https://images.alphacoders.com/478/478372.jpg')


# In[1]:


import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


# In[3]:


battles = pd.read_csv("/home/aarush100616/Downloads/Projects/Game of Thrones Analysis/battles.csv")
deaths = pd.read_csv("/home/aarush100616/Downloads/Projects/Game of Thrones Analysis/character-deaths.csv")


# In[4]:


battles.describe()


# In[5]:


battles = battles.drop(['defender_3','defender_4'],axis = 1)


# In[6]:


deaths.describe()


# In[7]:


#Checking the How many battles caused in which year
plt.subplot(2,1,1)
sns.countplot(battles['year'])
plt.title('How many battles caused in whuch year')
plt.show()


# In[8]:


#Checking the maximum death caused in whuch year
plt.subplot(2,1,2)
sns.countplot(deaths['Death Year'])
plt.title('How many death caused in whuch year')
plt.show()


# In[9]:


sns.countplot(deaths['Allegiances'])
plt.xticks(Rotation = 90)
plt.title('Which allegiances people died mostly')
plt.show()


# In[10]:


sns.countplot(deaths['Gender'])
plt.title('Which gender died most')
plt.xticks(np.arange(2),('Female','Male'))
plt.show()


# In[11]:


sns.countplot(x='attacker_outcome',data = battles)
plt.title('counting of attacker\'s outcome')
plt.show()


# In[12]:


#Lets see which king attacked the most
plt.figure(figsize=(10,10))
plt.subplot(2,1,1)
sns.countplot(x='attacker_king',data = battles)
plt.show()


# In[13]:


#Lets see which king defended the most
plt.figure(figsize=(15,10))
plt.subplot(2,1,2)
sns.countplot(x='defender_king',data = battles)
plt.show()


# In[14]:


plt.figure(figsize=(15,10))
plt.subplot(4,1,1)
sns.countplot(x= battles['attacker_1'],hue=battles['attacker_outcome'])
plt.show()


# In[15]:


plt.figure(figsize=(15,10))
plt.subplot(4,1,2)
sns.countplot(x= battles['attacker_2'],hue=battles['attacker_outcome'])
plt.show()


# In[16]:


plt.figure(figsize=(5,10))
plt.subplot(4,1,3)
sns.countplot(x= battles['attacker_3'],hue=battles['attacker_outcome'])
plt.show()


# In[17]:


plt.figure(figsize=(5,10))
plt.subplot(4,1,4)
sns.countplot(x= battles['attacker_4'],hue=battles['attacker_outcome'])
plt.show()


# In[18]:


plt.figure(figsize = (15,10))
plt.subplot(2,1,1)
sns.countplot(x= battles['defender_1'],hue=battles['attacker_outcome'])
plt.xticks(rotation  = 90)
plt.show()


# In[19]:


plt.figure(figsize = (3,5))
plt.subplot(2,1,2)
sns.countplot(x= battles['defender_2'],hue=battles['attacker_outcome'])
plt.show()


# In[20]:


sns.countplot(x= battles['battle_type'])
plt.title('Which battle type is done most?')
plt.show()


# In[21]:


sns.countplot(x= battles['battle_type'],hue=battles['attacker_outcome'])
plt.title('Type of battle VS Attacker_Outcome')
plt.show()


# In[22]:


sns.countplot(hue= battles['battle_type'],x=battles['attacker_king'],palette = 'Set3')
plt.title('Which type of battle used by attacker king ')
plt.legend(loc = 'upper right')
plt.xticks(rotation = 90)
plt.show()


# In[23]:


sns.barplot(x='attacker_outcome',y='attacker_size',data=battles)
plt.title('Attacker_outcome VS Attacker_size')
plt.show()


# In[24]:


sns.barplot(x='attacker_outcome',y='defender_size',data=battles)
plt.title('Attacker_outcome VS defender_size')
plt.show()


# In[25]:


sns.countplot(x='summer',data = battles)
plt.title('Counting of battles in summers')
plt.xticks(np.arange(2),('winters','summers'))
plt.show()


# In[26]:


sns.countplot(x='region', data = battles)
plt.title('In which region there are mostly battles done')
plt.xticks(rotation = 90)
plt.show()


# In[27]:


sns.countplot(x='region',hue='attacker_king', data = battles)
plt.title('Which king attacked which region')
plt.xticks(rotation = 90)
plt.legend(loc = 'upper right')
plt.show()

